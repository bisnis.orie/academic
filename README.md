# Academic Project

This is a _mencongak_ project from Workshop Java, create simple API with framework Java Spring (tool: Java Spring Boot).

**DESIGN**
1. Mahasiswa
2. Dosen
3. Matakuliah

Dosen >< Mahasiswa = one to many

Dosen >< Matakuliah = one to one

**TASK:**
1. Get all Dosen beserta relasi masing2
```
http://localhost:1212/api/getAllLecturer
```
2. API save: data dosen, mahasiswa, matkul
```
http://localhost:1212/api/saveLecturer
http://localhost:1212/api/saveStudent
http://localhost:1212/api/saveSubject
```
3. Praktikkan native query: terserah, tapi harus ada sisi kompleksnya (join 3 table)
```
http://localhost:1212/api/getCustomLecturer
```


**How To**
1. Clone this project repository
2. Create new database **academic** in mysql
3. Import academic.sql
4. Open project with Intellij IDEA
5. Load maven project untill completed
6. Run

