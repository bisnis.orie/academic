package com.ist.academic.academic.repository;

import com.ist.academic.academic.model.CustomLecturer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

@Service
public class CustomLecturerDAO {

    @Autowired
    private EntityManager em;

    public List<CustomLecturer> findAll(){
        String nativeQuery = "SELECT \n" +
                "    s.id as id, \n" +
                "    s.name as student_name, \n" +
                "    l.name as lecturer_name, \n" +
                "    j.name as subject_name \n" +
                "FROM student s \n" +
                "LEFT JOIN lecturer l on l.id = s.lecturer_id\n" +
                "INNER JOIN subject j on l.id = j.lecturer_id";

        Query q = em.createNativeQuery(nativeQuery, "CustomLecturerEntity");
        return q.getResultList();
    }

}
