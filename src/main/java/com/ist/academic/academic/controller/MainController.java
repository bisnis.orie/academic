package com.ist.academic.academic.controller;

import com.ist.academic.academic.model.CustomLecturer;
import com.ist.academic.academic.model.Lecturer;
import com.ist.academic.academic.model.Student;
import com.ist.academic.academic.model.Subject;
import com.ist.academic.academic.repository.CustomLecturerDAO;
import com.ist.academic.academic.repository.LecturerRepository;
import com.ist.academic.academic.repository.StudentRepository;
import com.ist.academic.academic.repository.SubjectRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@Slf4j
@RequestMapping(path = "/api")
public class MainController {

    @Autowired
    private LecturerRepository lecturerRepository;

    @Autowired
    private StudentRepository studentRepository;

    @Autowired
    private SubjectRepository subjectRepository;

    @Autowired
    private CustomLecturerDAO customLecturerDAO;

    // LECTURE: get all data
    @GetMapping("/getAllLecturer")
    public ResponseEntity<List<Lecturer>> getAllLecturer(){
        List<Lecturer> result = lecturerRepository.findAll();
        if (result == null || result.isEmpty()){
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    // LECTURE: Save
    @PostMapping("/saveLecturer")
    public ResponseEntity<Lecturer> saveLecturer(@RequestBody Lecturer lecturer){
        try{
            Lecturer l = lecturerRepository.save(lecturer);
            return new ResponseEntity<>(l, HttpStatus.CREATED);
        } catch (Exception e) {
            log.error("Error saving lecturer {}", e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // LECTURE: Delete
    @DeleteMapping("/deleteLecturer")
    public ResponseEntity<String> deleteLecturer(@RequestParam String lectureId){
        try {
            lecturerRepository.deleteById(Long.valueOf(lectureId));
            return new ResponseEntity<>("Deleted", HttpStatus.OK);
        } catch (Exception e) {
            log.error("Error deleting lecturer {}", e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // SAVE: Student
    @PostMapping("/saveStudent")
    public ResponseEntity<Student> saveStudent(@RequestBody Student student){
        try{
            Student s = studentRepository.save(student);
            return new ResponseEntity<>(s, HttpStatus.CREATED);
        } catch (Exception e) {
            log.error("Error saving student {}", e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // SAVE: Subject
    @PostMapping("/saveSubject")
    public ResponseEntity<Subject> saveSubject(@RequestBody Subject subject){
        try{
            Subject s = subjectRepository.save(subject);
            return new ResponseEntity<>(s, HttpStatus.CREATED);
        } catch (Exception e) {
            log.error("Error saving subject {}", e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // GET ALL DATA: Custom Native Query
    @GetMapping("/getCustomLecturer")
    public ResponseEntity<List<CustomLecturer>> getCustomLecturer(){
        List<CustomLecturer> result = customLecturerDAO.findAll();
        if (result == null || result.isEmpty()){
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @GetMapping("/test")
    public String test(){
        String message;
        message = "<code>" +
                "<b>Test API service</b><br/>" +
                "If this message show in your page browser, the web service is run successfully. \t&#128526; " +
                "</code>";
        return message;
    }

}
