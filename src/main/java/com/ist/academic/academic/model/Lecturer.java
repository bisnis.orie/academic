package com.ist.academic.academic.model;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
@Table(name = "lecturer")
public class Lecturer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;
    private String lecturerNumber;
    private String gender;

    @OneToMany(mappedBy = "lecturer")
    private List<Student> students;

    @OneToOne(mappedBy = "lecturer")
    private Subject subject;


}
