package com.ist.academic.academic.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "subject")
public class Subject {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;
    private Integer sks;
//    private Long lecturerId;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "lecturerId")
    @JsonBackReference
    private Lecturer lecturer;

}
