package com.ist.academic.academic.model;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@SqlResultSetMapping(name = "CustomLecturerEntity", entities = {
        @EntityResult( entityClass = CustomLecturer.class, fields = {
                @FieldResult(name = "id", column = "id"),
                @FieldResult(name = "studentName", column = "student_name"),
                @FieldResult(name = "lecturerName", column = "lecturer_name"),
                @FieldResult(name = "subjectName", column = "subject_name"),
        })
})
public class CustomLecturer {

    @Id
    private String id;
    private String studentName;
    private String lecturerName;
    private String subjectName;

}
