-- phpMyAdmin SQL Dump
-- version 4.9.5deb2
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Oct 17, 2021 at 03:54 PM
-- Server version: 10.3.31-MariaDB-0ubuntu0.20.04.1
-- PHP Version: 7.4.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `academic`
--

-- --------------------------------------------------------

--
-- Table structure for table `lecturer`
--

CREATE TABLE `lecturer` (
  `id` bigint(20) NOT NULL,
  `name` varchar(150) NOT NULL,
  `lecturer_number` varchar(25) NOT NULL,
  `gender` enum('male','female') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `lecturer`
--

INSERT INTO `lecturer` (`id`, `name`, `lecturer_number`, `gender`) VALUES
(1, 'Nisa M. Kom.', '202110131211', 'female'),
(2, 'Wahyudi M. Kom.', '202110131212', 'male'),
(3, 'Nisa Humairah M. Kom.', '202110131213', 'female'),
(4, 'Bety Wulan M. Kom.', '202110131214', 'female'),
(5, 'Tri Wahyudi M. Kom.', '202110131215', 'male');

-- --------------------------------------------------------

--
-- Table structure for table `student`
--

CREATE TABLE `student` (
  `id` bigint(20) NOT NULL,
  `name` varchar(150) NOT NULL,
  `student_number` varchar(25) NOT NULL,
  `gender` enum('male','female') NOT NULL,
  `lecturer_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `student`
--

INSERT INTO `student` (`id`, `name`, `student_number`, `gender`, `lecturer_id`) VALUES
(1, 'Andrian Cahya', '21.10.0001', 'male', 1),
(2, 'Malik Hidayat', '21.10.0002', 'male', 1),
(3, 'Renata', '21.10.0003', 'female', 1),
(4, 'Mayang', '21.10.0004', 'female', 2),
(5, 'Teguh', '21.10.0005', 'male', 2),
(6, 'Wicaksono', '21.10.0006', 'male', 3),
(7, 'Ari', '21.10.0007', 'male', 3),
(8, 'Dibaj Tri', '21.10.0008', 'female', 4),
(9, 'Widya', '21.10.0009', 'female', 4);

-- --------------------------------------------------------

--
-- Table structure for table `subject`
--

CREATE TABLE `subject` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `sks` smallint(2) NOT NULL,
  `lecturer_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `subject`
--

INSERT INTO `subject` (`id`, `name`, `sks`, `lecturer_id`) VALUES
(1, 'Bahasa Pemrograman', 3, 1),
(2, 'Pengantar Sistem Informasi', 3, 2),
(3, 'Aljabar Linear', 3, 3),
(4, 'Perancangan & Pemrograman Web', 3, 4);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `lecturer`
--
ALTER TABLE `lecturer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `student`
--
ALTER TABLE `student`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subject`
--
ALTER TABLE `subject`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `lecturer`
--
ALTER TABLE `lecturer`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `student`
--
ALTER TABLE `student`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `subject`
--
ALTER TABLE `subject`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
